#include "HexaSerial.h"

HexaSerialProtocol::HexaSerialProtocol() {
  index = 0;
}

void HexaSerialProtocol::init(int baudrate) {
	Serial.begin(baudrate);
}

char HexaSerialProtocol::getChar() {
	char cmd;

	if(!Serial.available()) {
		return 0; // no data available
	} else 
		cmd = Serial.read();
	
        return isHexaProt(cmd);
      
}


char HexaSerialProtocol::isHexaProt(char ch) {
  

	if(ch >= '0' && ch <= '9') return ch;

	if(ch >= 'A' && ch <= 'F') return ch;

        if(ch == '!' || ch == '#') return ch;
	
	return 0;

}


HexaSerialProtocol::~HexaSerialProtocol() {

}


void HexaSerialProtocol::addChar(char ch) {
      message[index] = ch;
      index++;
      message[index] = '\0';
}

void HexaSerialProtocol::reset() {
  
      index= 0;
 
}

char *HexaSerialProtocol::getMessage(){
  return message;
}

char *HexaSerialProtocol::getNumMess(){
  return numMess;
}

boolean HexaSerialProtocol::isMessage(){
  char ch;
  
  ch = getChar();
  
  if (ch){
    if (ch == '!'){
      reset();
      addChar(ch);
      return false;
      
    } else {
     if (index < 63){
           if (ch == '#'){
      
              addChar(ch);
              //  !ADCHCMLE#
              if (index > 10){
                return true;
              }
        }else{
          addChar(ch);
          return false;
      }
    } else {
       reset();
       return false;
    }
   }    
  }
};

/*
 *   Convert Hexa decimal ASCII character to numerical value
 */
void HexaSerialProtocol::messToNum (){
  int i;

  i = 1;
  while (message[i] != '#'){
    if (message[i] >= '0') 
	if (message[i] <= '9') numMess[i] = message[i] - '0';
    if (message[i] >= 'A') 
        if (message[i] <= 'F') numMess[i] = message[i] - 'A' + 10;
    i++;
  }
}


/*
 *   Convert Hexa decimal ASCII character to numerical value
 */
void NumMessage::hexToNum (){
  int i;

  i = 1;
  while (message[i] != '#'){
    if (message[i] >= '0') 
	if (message[i] <= '9') numMess[i] = message[i] - '0';
    if (message[i] >= 'A') 
        if (message[i] <= 'F') numMess[i] = message[i] - 'A' + 10;
    i++;
  }
}



