#ifndef HEXASERIALPROTOCOL_H
#define HEXASERIALPROTOCOL_H

#include "def.h"

/*
*
*/

class NumMessage {
private:
  char * message;
  void hexToNum ();  
  char numMess[MAX_LENGTH];
public:
  NumMessage(char *);
  ~NumMessage();
};

class HexaSerialProtocol {
  
private:
  char isHexaProt(char);
  void addChar(char);
  char getChar();

  char message[MAX_LENGTH];
  char numMess[MAX_LENGTH];
  char index;

public:
  HexaSerialProtocol();
  ~HexaSerialProtocol();
  
  void reset() ; 
  void init(int baudrate);
  boolean isMessage();
  char * getMessage();
  char * getNumMess();
  /*Convert HEX message to NUM message*/
  void messToNum ();  
};

#endif

