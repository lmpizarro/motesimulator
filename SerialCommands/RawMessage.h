#ifndef RAWMESSAGE2_H
#define RAWMESSAGE2_H

/*
*
*/

const unsigned char HEADER_LENGTH = 10;
const unsigned char MAX_PAYLOAD  =  7;
const unsigned char MAX_COMMANDS  =  4;

class RawMessage {
  private:
    unsigned char error;
  public:
    unsigned char address;
    unsigned char channel;
    unsigned char command;
    unsigned char payloadSize;
    unsigned char cks;
    unsigned int payload [MAX_PAYLOAD];
    /*Convert NUM message to Raw Message
     * Checks payloadSize and checksum
     */
    unsigned char nMessToRMess (unsigned char * numMess);
    /*Verify CheckSum*/
    unsigned char checkCks ();
    void rMessToHexMess (unsigned char * message);
    void setPayload(unsigned int, unsigned char);
    
    unsigned char calcCks();
    unsigned char getError();
    RawMessage(unsigned char, unsigned char, unsigned char, unsigned char);
    RawMessage(unsigned char *);
    RawMessage();   
    ~RawMessage();
};
#endif
