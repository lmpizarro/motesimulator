# MoteSimulator: 

## A master slave protocol for an RF serial communication to adquire process values

A protocol to simulate a simple Wireless Sensor Network

run:

> pip install -r requirements.txt

install crosbar

run:

in on terminal:

> crossbar start

in another terminal:

> python backend.py
 

in another terminal:

> python frontend.py


See:

[virtualenvs](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

[Autobahn|Python](http://autobahn.ws/python/)

[crossbarexamples](https://github.com/crossbario/crossbarexamples)

[Arduino](https://www.arduino.cc/)

[git commands](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)
