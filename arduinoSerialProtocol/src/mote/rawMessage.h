#ifndef RAWMESSAGE_H
#define RAWMESSAGE_H

/*
*
*/

typedef struct rawMessages {
  unsigned char address;
  unsigned char channel;
  unsigned char command;
  unsigned char payload;
  unsigned char cks;
} rawMessage;



/*Convert NUM message to Raw Message*/
void nMessToRMess (rawMessage * rm, char * message);
/*Verify CheckSum*/
char checkCks (rawMessage * rm);

//void rMessToHexMess (rawMessage *rm, char * message);

#endif
