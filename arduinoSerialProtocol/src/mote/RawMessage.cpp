#include "RawMessage.h"

RawMessage::RawMessage(unsigned char add, unsigned char chan, 
		       unsigned char cmd, unsigned char pls){
   int i;

   error = 0;

   for(i=0; i<MAX_PAYLOAD; i++) payload[i] = 0;

   address = add;
   channel = chan;
   command = cmd;
   if (pls <= MAX_PAYLOAD){
      payloadSize = pls;
   }
   else{
      payloadSize = MAX_PAYLOAD;
      error += 1;
   }
}

RawMessage::RawMessage(){
  error = 0;
  address = 0;
  channel = 0;
  command = 0;
  payloadSize = 0;
  
  for(int i=0; i<MAX_PAYLOAD; i++) payload[i] = 0;
}

RawMessage::RawMessage(unsigned char *message){
  int i, n; 

  error = 0;

  address = message[1] * 16 + message [2];
  channel = message[3] * 16 + message [4];
  command = message[5] * 16 + message [6];
  payloadSize = message[7] * 16 + message [8];

  i = 9;
  n = 0;
  while (message[i++] != '#'){
     n++;
  }

  if (n != payloadSize * 4 + 2) error +=1;

  n = 0;
  for(i=0; i < payloadSize * 4; i+=4){
     payload[n] = message[i+9] * 16*16*16 + message [i+10] * 16 * 16 + 
	     message[i+11] * 16 + message[i+12];
     n++;
  }

  cks = message[payloadSize * 4 + 9] * 16 + message [payloadSize * 4 + 10];
  
  if (calcCks() != cks) error +=1;
}

RawMessage::~RawMessage(){

}

unsigned char RawMessage::calcCks() {
  unsigned char tmp;	
  tmp = command + address + payloadSize + channel; 
  
  for (int i = 0; i < payloadSize; i++)
    tmp += payload[i];
  return tmp;
}

void RawMessage::setPayload(unsigned int value, unsigned char pos){
  if (pos < payloadSize)	
    payload[pos] = value;
}

/*
 *   unsigned char mess [] = {'!', 0, 1, 0, 1, 0, 1, 0, 2, 15, 15, 
 *	                    15, 15, 15, 15, 15, 14 ,0, 5, '#'};
 *
 * 
 */
unsigned char RawMessage::nMessToRMess (unsigned  char * message){
  int i, n; 

  address = message[1] * 16 + message [2];
  channel = message[3] * 16 + message [4];
  command = message[5] * 16 + message [6];
  payloadSize = message[7] * 16 + message [8];

  i = 9;
  n = 0;
  while (message[i++] != '#'){
     n++;
  }

  if (n != payloadSize * 4 + 2) return (0);

  n = 0;
  for(i=0; i < payloadSize * 4; i+=4){
     payload[n] = message[i+9] * 16*16*16 + message [i+10] * 16 * 16 + 
	     message[i+11] * 16 + message[i+12];
     n++;
  }

  cks = message[payloadSize * 4 + 9] * 16 + message [payloadSize * 4 + 10];


  if (calcCks() != cks) 
    return (0);
  else
    return (1);
}

unsigned char RawMessage::checkCks (){
   if (calcCks() == cks) return 1;
   return 0;
}

/*
 * Convert a RawMessage to an HexaDecimal message
 *
 */
void RawMessage::rMessToHexMess (unsigned char * message){
  int i;

  message[0] = '!';
  message[1] = ((15 << 4) & address)>>4;
  message[2] = 15 & address;
  message[3] = ((15 << 4) & channel)>>4;
  message[4] = 15 & channel;
  message[5] = ((15 << 4) & command)>>4;
  message[6] = 15 & command;
  message[7] = ((15 << 4) & payloadSize)>>4;
  message[8] = 15 & payloadSize;
  // TODO

  message[9 + payloadSize * 4] = ((15 << 4) & cks)>>4;
  message[10 + payloadSize * 4] = 15 & cks;
  message[payloadSize * 4 +  11] = '#';

  for (i = 1; i < 10 + payloadSize * 4 ; i++){
      if (message[i] >=0 && message[i] <= 9) message[i] += '0';
      if (message[i] >=10 && message[i] <= 15) message[i] += 'A' - 10;
  }
}


unsigned char RawMessage::getError(){
  if (error != 0) 
    return 1; /*There is an error*/
  else 
    return 0;
}
