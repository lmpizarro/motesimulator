#include "rawMessage.h"


void nMessToRMess (rawMessage * rm, char * message){

	rm->address = message[1] * 16 + message [2];
	rm->channel = message[3] * 16 + message [4];
	rm->command = message[5] * 16 + message [6];
	rm->payload = message[7] * 16 + message [8];
	rm->cks = message[9] * 16 + message [10];
}

char checkCks (rawMessage * rm){
   unsigned char cks;

   cks = rm->address + rm->channel + rm->command;

   if (cks == rm->cks) return 1;
   return 0;
}
