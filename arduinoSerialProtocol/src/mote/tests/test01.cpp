/*
 *
 * Tests for classes to use in Arduino
 *
 * g++ -c RawMessage.cpp
 * g++ -c test.cpp
 * g++ -o test test.o RawMessage.o
 *
 *
 */

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#include "RawMessage.h"

void outRm (RawMessage rm){
  int i;
  cout << "address: " << (int)rm.address<< endl;
  cout << "channel: " << (int)rm.channel<< endl;
  cout << "command: " << (int)rm.command<< endl;
  cout << "payloadSize: " << (int)rm.payloadSize<< endl;

  for (i = 0; i < rm.payloadSize; i++)
	  cout << "payload: " << i << " " << rm.payload[i] << endl;

  cout << "cks: " << (int)rm.cks<< endl;

}

int main (){
  int i; 

  unsigned char mess [] = {'!', 0, 1, 0, 1, 0, 1, 0, 2, 15, 15, 
	                    15, 15, 15, 15, 15, 14 ,0, 5, '#'};

  //    addres, channel, command, payloadSize  
  //RawMessage rm(1, 2, 1, 1);
  RawMessage rm(mess);

  cout << "Error: " << (int) rm.getError() << endl;

  rm.calcCks();

  outRm(rm);

  rm.setPayload(20, 0);

  for (i = 0; i < rm.payloadSize; i++)
	  cout << "payload: " << i << " " << rm.payload[i] << endl;

  rm.calcCks();

  cout << "cks: " << (int)rm.cks<< endl;

  cout << "salida: " << (int) rm.nMessToRMess(mess) << endl;

  outRm(rm);

  return 1;
}
