#include <stdio.h>
#include "rawMessage.h"

int main(){

  rawMessage rm;	

  char message [] = {'!','0','F','0','1','0','1','0', '0','1','1','#','\0'};

  int i;

  printf ("%s\n", message);

  messToNum(message);

  i = 1;
  while (message[i] != '#'){
    printf ("%d\n", message[i]);
    i++;
  } 

  printf ("\n\n");

  nMessToRMess (&rm,  message);

  printf("%d\n", rm.address);
  printf("%d\n", rm.channel);
  printf("%d\n", rm.command);
  printf("%d\n", rm.payload);
  printf("%d\n", rm.cks);
  
  printf ("\n\n");

  printf("%d\n", checkCks(&rm));

  return 1;
}
