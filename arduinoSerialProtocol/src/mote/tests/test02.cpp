#include <iostream>
using namespace std;


#include "RawMessage.h"

int main (){
  RawMessage rm;
  unsigned char numMess [] = {'!', 0, 1, 0, 1, 0, 1, 0, 0, 0,3,'#'};

  rm.nMessToRMess(numMess);

  cout << "test: " << "\n";
  cout << (int) rm.address << endl;
  cout << (int) rm.channel << endl;
  cout << (int) rm.command << endl;
  cout << (int) rm.payload << endl;
  cout << (int) rm.cks << endl;

}
