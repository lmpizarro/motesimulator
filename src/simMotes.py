from random import randint
import motesDb
import utils as ut

class GwMessages:
    def __init__(self):
        pass

    # !AANNCCCK#
    def getToSend(self, adrMot, motChan, command):
        """ A message to send to the mote"""
        cks = "EF"
        mess = "!" + adrMot + motChan + command + cks + "#"
        return mess

class SimMotes:
    def __init__(self):
        self.sDb = motesDb.MotesDb()
        
    def message(self, addr):
        """  generates a  simulated message """
        na = str(hex(self.sDb.motes[addr]['NA']))[2:].zfill(2).upper()
        values = na 
        for i in range(self.sDb.motes[addr]['NA']):
            values += str(hex(randint(0, 65535)))[2:].zfill(4).upper()

        return "!"+ addr + values + "#"

   
    def decode(self, mess):
        """ decodes a message of the form !0304ffffddddeeee# """
        len_mess = len(mess)
        values = []
        decoded = {}
        if len_mess >= 2:
            if mess[0] == '!' and mess[len_mess - 1] == '#':
                mess =  mess[1:len_mess - 1]
                # at least !AA021111#
                if len(mess) >=8:
                    if ut.in_hexas(mess):
                        adr = mess[0:2] # key to sensors?
                        ply = mess[2:4] # payload?
                        ply = int(ply, 16)
                        adr = int(adr, 16)
                        data = mess[4:]
                        decoded["ADR"] = adr
                        decoded["NA"] = ply
                        for i in range(ply):
                            values.append( int(data[4*(i): 4*(i+1)], 16))
                        decoded["VALS"] = values
                        decoded["STA"] =  True
                    else:
                        decoded["STA"] =  True
                else:
                        decoded["STA"] =  False
            else:
                        decoded["STA"] =  False
        else:
                        decoded["STA"] =  False

        return decoded
        # a dict with STA in True or False and a list of VALS

def test():
    snrs = SimSensors()
    mes = snrs.message('01')

    mes = "FE1000V"


    print "mal ", snrs.decode("")
    print "mal ", snrs.decode("FF")
    print "mal ", snrs.decode("!FF#")
    print "mal ", snrs.decode("!FF01#")
    print "bien ", snrs.decode("!FF01FFFF#")
    print "bien ", snrs.decode("!0F02FFFFEEEE#")


def test02():
    dm1 = MessageDecoder()
    mess = "!AAEEDD011111EE#"

    if dm1.decode(mess):
        print dm1
        print mess

    dm4 = MessageDecoder()
    mess = "!AAEEDD041111222233334444EE#"

    if dm4.decode(mess):
        print dm4
        print mess
        dm4.valuesToString()

    mess = "!AAEEDD04111122223333EE#"

    if dm4.decode(mess):
        print dm4
        print mess
        dm4.valuesToString()

    else:
        print "Error"

if __name__ == '__main__':
    pass
    test02()
