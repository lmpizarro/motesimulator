import utils as ut

class RawMessage:
    def __init__(self, addr=1, chan=1, command=1):
        self.chan = chan
        self.addr = addr
        self.command = command
        self.payload = 0 
        self.values = []
        self.ck =  self.chan + self.addr + self.command + self.payload

    def calcCk(self):
            self.ck =  self.chan + self.addr + self.command + self.payload
            for v in self.values:
                self.ck += v

    def addValues(self, values=[0]):
        for value in values:
            if value <= 65535 and value > 0:
                self.values.append(int(value))
            else:
                self.values.append(0)

        self.payload = len(self.values)

        self.calcCk()


    def changeValue(self, value, indx):
        if value <= 65535 and value > 0:
            if indx < len(self.values):
                self.values[indx] = value

        self.calcCk()

    def setCommand(self, command):
        self.command = command
        self.calcCk()

    def setAddr(self, addr):
        self.addr = addr
        self.calcCk()

    def setChannel(self, chan):
        sef.chan = chan
        self.calcCk()

    def valuesToString(self):
        tmp = ""
        for i, d in enumerate(self.values):
            tmp += "  val" + str(i) +  " " + str(d) + "\n"

        return tmp


    def __str__(self):
        tmp =  "adr: "+ str(self.addr) + " chan: "+ str(self.chan) + \
               " cmd: "+ str(self.command) + "\ndata:\n"
          
        tmp += self.valuesToString()  
                    
        tmp += "cks: "+ str(self.ck)

        return tmp


class MessageEncoder:
    def __init__(self, rawMessage):

        self.rm = rawMessage
  

        self.message = ""
        self.setMessage()


    def setHeader(self):
        self.PL = ut.intToHex(self.rm.payload, 2)
        self.message = '!' + ut.intToHex(self.rm.addr, 2) + ut.intToHex(self.rm.chan, 2) + \
                ut.intToHex(self.rm.command, 2) + self.PL


    def setMessage (self):

        self.setHeader()

        for v in self.rm.values:
            self.message += ut.intToHex(v, 4)


        self.message += ut.intToHex(self.rm.ck, 2) + "#"

    def setRawMessage(self, rawMessage): 
        self.rm = rawMessage
        self.setMessage()


    def __str__ (self):
        return self.message


class MessageDecoder:
    def __init__(self):
        self.values = []
        self.rm = RawMessage()

    def decode(self, mess):
        """ decodes a message of the form !AACHCMPL<PL*4hexas>CK# 
        AA: addr
        CH: channel
        CM: command
        PL: n hexas
        payload*4hexas: int values btw 0-65535 in hexa 0-FFFF 
        CK: checksum
        """
        len_mess = len(mess)
        if len_mess >= 16:
            if mess[0] == '!' and mess[len_mess - 1] == '#':
                mess =  mess[1:len_mess - 1]
                # at least !AA021111#
                if len(mess) >=8:
                    if ut.in_hexas(mess):

                        self.rm.adr = ut.hexToInt(mess[0:2]) # 
                        self.rm.chan = ut.hexToInt(mess[2:4])# 
                        self.rm.command = ut.hexToInt(mess[4:6])
                        self.PL = mess[6:8]

                        self.rm.payload = ut.hexToInt(self.PL)

                        if len(mess) == 8 + 4*self.rm.payload + 2:
                            self.data = mess[8: 8 + self.rm.payload*4]

                        for i in range(self.rm.payload):
                            self.rm.values.append(ut.hexToInt(self.data[i*4:4*(i+1)]))

                        self.rm.ck = ut.hexToInt(mess[8 + self.rm.payload*4:8 + self.rm.payload*4 + 2])
                        return  True
                    else:
                       return False 
                else:
                       return False
            else:
                       return False
        else:
                       return False

    def valuesToString(self):
        tmp = ""
        for i, d in enumerate(self.values):
            tmp += "  val" + str(i) +  " " + d + "\n"

        return tmp

    def __str__(self):
        return self.rm.__str__()


def test03():
    rwm = RawMessage()

    rwm.addValues([1, 2, 3])

    print "first ", rwm

    dm1 = MessageEncoder(rwm)
    print dm1


    dm2 = MessageDecoder()


    rwm.changeValue(3, 0)

    dm1.setRawMessage(rwm)

    print "changed: ", dm1



    # !010101030002000200010b#
    if dm2.decode(dm1.message):
        print dm2
    else:
        print "Error decode"

    '''
    '''

if __name__ == '__main__':
    pass
    test03()
