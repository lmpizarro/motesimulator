from os import environ
from twisted.internet import task
from twisted.internet import reactor
from autobahn.twisted.util import sleep
from twisted.internet.defer import inlineCallbacks, Deferred
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
import simMotes
import time

import pymongo

# Connection to Mongo DB
try:
        conn=pymongo.MongoClient()
        print "Connected successfully!!!"
except pymongo.errors.ConnectionFailure, e:
       print "Could not connect to MongoDB: %s" % e 


db = conn.my_messages
collection = db.my_collection


import moteScheduler

class MyComponent(ApplicationSession):
    snrs = simMotes.SimMotes()
    moteSch = moteScheduler.Scheduler(snrs.sDb)
    gwMessages = simMotes.GwMessages()

    @inlineCallbacks
    def onJoin(self, details):
        while True: 
            adq = self.moteSch.increment_time()
            if len(adq) != 0:
                for e in adq:
                    print "salida adq", e
                    print e['MOT'], e['CHAN']
                    mess = self.gwMessages.getToSend(e['MOT'], e['CHAN'], '01')
                    res =  yield self.call(u'com.myapp.sensor', e['MOT'])
                    print ("my sensor: {} {}".format(res, mess))
            yield sleep(1)

if __name__ == '__main__':
    runner = ApplicationRunner(
        environ.get("AUTOBAHN_DEMO_ROUTER", u"ws://127.0.0.1:8080/ws"),
        u"crossbardemo",
    )

    runner.run(MyComponent)
