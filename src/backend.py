from os import environ
from twisted.internet.defer import inlineCallbacks
from twisted.internet.task import LoopingCall
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
import simMotes

class MyComponent(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
        # a remote procedure; see frontend.py for a Python front-end
        # that calls this. Any language with WAMP bindings can now call
        # this procedure if its connected to the same router and realm.

        sims = simMotes.SimMotes()

        # mess  !AANNCCCC#
        # TODO 
        def sensor (addr):
            """ """
            # addr is an hexa: 01<= addr <= FF
            print addr

            return sims.message (addr) 


        yield self.register(sensor, u'com.myapp.sensor')

if __name__ == '__main__':
    runner = ApplicationRunner(
        environ.get("AUTOBAHN_DEMO_ROUTER", u"ws://127.0.0.1:8080/ws"),
        u"crossbardemo",
    )
    runner.run(MyComponent)
