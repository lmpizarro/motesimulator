class Scheduler:
    def __init__(self, sensorsDb):
        """ init the Scheduler with a database of motes """
        self.sensorsDb = sensorsDb
        self.sKeys =  self.sensorsDb.motes.keys()
        print ' init sensors keys: ', self.sKeys


        # each chan of mote has a TO = 0
        for k in self.sKeys:
            for  ch in self.sensorsDb.motes[k]['CHANS']:
                ch['TO'] = 0


    def increment_time (self):    
        """ for each mote increment time in each channel """

        adq = []
        for mote  in self.sKeys:
            # for each chan in each mote
            if self.sensorsDb.motes[mote]['ACT'] == True:
                for i,ch  in  enumerate(self.sensorsDb.motes[mote]['CHANS']):
                    # increment time for chan
                    ch['TO'] += 1
                    if ch['TO'] == ch['SCH']:
                        adq.append({'MOT': mote, 'CHAN':hex(i)[2:].zfill(2)})
                        ch['TO'] = 0

        return adq                
