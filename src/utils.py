hexas = ['0', '1','2', '3','4', '5','6', '7','8', '9','A', 'B','C', 'D', 'E', 'F']

def in_hexas (mess):
    """ determines if all caracters in the string are hexas """
    for c in mess:
        if c not in hexas:
            return False
    return True     

def intToHex(intN, fill):
    return hex(intN)[2:].zfill(fill).upper()

def hexToInt(hex_):
    return int(hex_, 16)
