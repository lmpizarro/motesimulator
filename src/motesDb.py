import time 
import moteScheduler

# Properties of mote's channels
chans = {
        '01':[{'SCH': 6, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}, {'SCH': 9, 'CMD':'adq'}, {'SCH': 4, 'CMD':'adq'}], 
        '03':[{'SCH': 6, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}], 
        '06':[{'SCH': 6, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}, {'SCH': 9, 'CMD':'adq'}, 
            {'SCH': 6, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}], 
        '05':[{'SCH': 7, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}, {'SCH': 9, 'CMD':'adq'}, 
            {'SCH': 7, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}, {'SCH': 9, 'CMD':'adq'}], 
        '08':[{'SCH': 7, 'CMD':'adq'}], 
        '09':[{'SCH': 7, 'CMD':'adq'}, {'SCH': 5, 'CMD':'adq'}, {'SCH': 9, 'CMD':'adq'}], 
        }

# Mote's location
location = {
            '01': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            '03': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            '06': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            '05': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            '08': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            '09': {'geometry':{'type': 'Point', 'coordinates': [1.0, 1.0]}, 
                  'properties': {'DES':'...'}}, 
            }

# Mote's
motes =  {
          '01': {'ADR': 1, 'NA': 4, 'ACT':True, 'SCH': 100, 'CMD':'soh'}, 
          '03': {'ADR': 3, 'NA': 2, 'ACT':True, 'SCH': 120, 'CMD':'soh'}, 
          '06': {'ADR': 6, 'NA': 5, 'ACT':True, 'SCH': 140, 'CMD':'soh'},
          '05': {'ADR': 5, 'NA': 6, 'ACT':True, 'SCH': 160, 'CMD':'soh'},
          '08': {'ADR': 8, 'NA': 1, 'ACT':True, 'SCH': 180, 'CMD':'soh'},
          '09': {'ADR': 9, 'NA': 3, 'ACT':True, 'SCH': 200, 'CMD':'soh'},
          }

class Channel:
    def __init__(self, indx, id_, sched, command, desc=""):
        self.indx = indx
        self.id_ = id_
        self.sched = sched
        self.command = command
        self.desc = desc

class Mote:
    def __init__(self, indx, adr, act, chans=[], location=None):
        """init the mote with and adress, and active state """
        self.indx = indx
        self.adr = adr
        self.act = act
        self.chans = chans
        self.location = location

    def addChan(self, chan):
        """ add chan info """
        self.chans.append(chan)
        self.na = len(self.chans)

    def setLocation(self, location):
        """ set the mote location"""
        self.location = location

    def getLocation(self):
        """ get the mote location """
        return self.location

    def getAddres(self):
        """ return the mote adress"""
        return self.adr
#
# TODO: define commands
# commands for channels: CMD = "adq", CMD = ... to be determined
#
class MotesDb:
    global motes, location, chans

    def __init__(self):
        """ creates the motes Db  """
        self.motes = motes 
        keys = self.motes.keys()

        for k in keys:
            self.motes[k]['LOC'] = location[k]
            self.motes[k]['CHANS'] = chans[k]

def test02 ():
    sDb = MotesDb()
    moteSch = moteScheduler.Scheduler(sDb)

    for time_ in range(20):
        pass
        print time_, "\n\n"
        adq = moteSch.increment_time()
        if len(adq) != 0:
            print adq
        time.sleep(1.0)


if __name__ == '__main__':
    pass
    test02()
